define([], function () {
    VT_OVERLAY = ' \
<div class="overlay-inner"> \
  <div class="container-fluid"> \
    <div class="row"> \
      <div id="feedback-message" class="col-xs-12"> \
      </div> \
    </div> \
    <div class="row"> \
      <div id="visualization-tool" class="col-xs-12"> \
        <style type="text/css"> \
          .recline-slickgrid { \
            height: 600px; \
          } \
   \
          .changelog { \
            display: none; \
            border-bottom: 1px solid #ccc; \
            margin-bottom: 10px; \
          } \
        </style> \
   \
        <div class="changelog"> \
          <h3>Changes</h3> \
        </div> \
   \
        <div class="data-explorer"></div> \
        <div style="clear: both;"></div> \
      </div> \
    </div> \
  </div> \
</div> \
';
return window.VT_OVERLAY = VT_OVERLAY;
 });
