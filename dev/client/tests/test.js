var expect = chai.expect;
/*var should = chai.should;*/

describe('Client suite', function() {
   

  before(function() {
   
  });

  after(function() {

  });

  describe('Backends formatting', function() {

    it('should set correct type when reading number from XML', function(done) {
      this.timeout(20000);
      
      var server = sinon.fakeServer.create();
       
      server.respondWith('GET', '/fake/file.xml', [
        200, 
        {'Content-Type': 'application/text'}, 
        '<tests><test><a>string</a><b>number</b><c>number2</c></test><test><a>bla, bla</a><b>2435.6</b><c>2,456,789.3456</c></test></tests>'
      ]);
       
      var dataset = snippetHelper.createDataset ('/fake/file.xml', 'xml') ;

      var promise = dataset.fetch();
      //expect(dataset.data).to.equal(undefined);
      promise.then(function(){
        expect(dataset.fields.length).to.equal(3);
        expect(dataset.records.models[1].attributes.a).not.to.be.a('number');
        expect(dataset.records.models[1].attributes.b).to.be.a('number');
        expect(dataset.records.models[1].attributes.c).to.be.a('number');
        done();
      });

      server.respond();   
      server.restore();
    });

    it('should set correct type when reading number from CSV', function(done) {
      this.timeout(20000);
      
      var server = sinon.fakeServer.create();
       
      server.respondWith('GET', '/fake/file.csv', [
        200, 
        {'Content-Type': 'application/text'}, 
        'a,b,c\nbla bla,3513.456,"6,123,345.234"'
      ]);
       
      var dataset = snippetHelper.createDataset ('/fake/file.csv', 'csv') ;

      var promise = dataset.fetch();
      //expect(dataset.data).to.equal(undefined);
      promise.then(function(){
        expect(dataset.fields.length).to.equal(3);
        expect(dataset.records.models[0].attributes.a).not.to.be.a('number');
        expect(dataset.records.models[0].attributes.b).to.be.a('number');
        expect(dataset.records.models[0].attributes.c).not.to.be.a('number');
        done();
      });

      server.respond();   
      server.restore();
    });
  });  


  describe('appendToList', function() {
    beforeEach(function(){
      $('#sheet_names').html('');
    });

    it('should populate an select element with sheet names', function() {
      $('<select id="sheet_names">').appendTo('body');
      snippetHelper.appendToList(['a', 'b']);
      expect($('#sheet_names').html()).to.equal('<option value="Select a sheet">Select a sheet</option><option value="a">a</option><option value="b">b</option>');
    });

    it('should call a fake server to get sheet names for fake file', function(done) {
      
      var server = sinon.fakeServer.create();
       
      server.respondWith('GET', 'fake/file.csv', [
        200, 
        {'Content-Type': 'application/json'}, 
        '["c","d"]'
      ]);
       
      $.get('fake/file.csv', function (data) {
        snippetHelper.appendToList(data);
        expect($('#sheet_names').html()).to.equal('<option value="Select a sheet">Select a sheet</option><option value="c">c</option><option value="d">d</option>');
        done();
      });
      server.respond();
      server.restore();
    });
  });

  describe('createDataset through fake server', function() {
    it('should call a fake server to get data of a fake CSV file', function(done) {

      var server = sinon.fakeServer.create();
       
      server.respondWith('GET', '/fake/file.csv', [
        200, 
        {'Content-Type': 'application/text'}, 
        'x,y,z\n1,2,3\n4,5,6'
      ]);
       
      var dataset = snippetHelper.createDataset ('/fake/file.csv', 'csv') ;

      var promise = dataset.fetch();
      //expect(dataset.data).to.equal(undefined);
      promise.then(function(){
        expect(dataset.fields.length).to.equal(3);
        done();
      });

      server.respond();
      server.restore();
    });
    
    it('should call a fake server to get data of a fake XML file', function(done) {

      var server = sinon.fakeServer.create();
       
      server.respondWith('GET', '/fake/file.xml', [
        200, 
        {'Content-Type': 'application/text'}, 
        '<tests><test><a>1</a><b>2</b><c>3</c><d>4</d></test></tests>'
      ]);
       
      var dataset = snippetHelper.createDataset ('/fake/file.xml', 'xml') ;

      var promise = dataset.fetch();
      //expect(dataset.data).to.equal(undefined);
      promise.then(function(){
        expect(dataset.fields.length).to.equal(4);
        done();
      });

      server.respond();   
      server.restore();
    });

    it.skip('should call a fake server to get data of a fake RDF file', function(done) {

      var server = sinon.fakeServer.create();
       
      server.respondWith('GET', '/fake/file.xml', [
        200, 
        {'Content-Type': 'application/text'}, 
        '<rdf:tests><rdf:test><rdf:a>1</rdf:a><rdf:b>2</rdf:b><rdf:c>3</rdf:c><rdf:d>4</rdf:d></rdf:test></rdf:tests>'
      ]);
       
      var dataset = snippetHelper.createDataset ('/fake/file.xml', 'xml') ;

      var promise = dataset.fetch();
      //expect(dataset.data).to.equal(undefined);
      promise.then(function(){
        expect(dataset.fields.length).to.equal(4);
        done();
      });

      server.respond();   
      server.restore();
    });

      

  });

  describe('buildPage', function() {
    beforeEach(function(){
        $('.data-explorer').html('');
    });
    it('should create a Recline dataset from fake data and display the results in the web page', function(done) {
      this.timeout(4000);

      
      var server = sinon.fakeServer.create();
       
      server.respondWith('GET', '/fake/file.csv', [
        200, 
        {'Content-Type': 'application/text'}, 
        'x,y,z\n1,2,3\n4,5,6'
      ]);

      window.explorerDiv = $('.data-explorer');
      snippetHelper.buildPage (
          '/fake/file.csv',
          'csv'
      ) ;
      server.respond();

      setTimeout(function(){
        var xElement = $(window.explorerDiv.find('.slick-header-column')[1]).find('.slick-column-name');
        expect(xElement.html()).to.equal('x');
        done();
      }, 500);
       
      server.restore();

    });

    it('should create a Recline dataset from fake data and display the results with the correct default decimal and thousand separators', function(done) {
      this.timeout(4000);

      
      var server = sinon.fakeServer.create();
       
      server.respondWith('GET', '/fake/file.csv', [
        200,
        {'Content-Type': 'application/text'}, 
        'x,y,z\nbla bla,3513.456,"6,123,345.234"\n1,2,3'
      ]);

      window.explorerDiv = $('.data-explorer');
      snippetHelper.buildPage (
          '/fake/file.csv',
          'csv'
      ) ;
      server.respond();

      setTimeout(function(){
        expect(window.explorerDiv.find('.even').find('.r2').html()).to.equal('3,513.456');
        done();
      }, 500);
       
      server.restore();

    });

    it('should create a Recline dataset from fake data and display the results with the correct given decimal and thousand separators', function(done) {
      this.timeout(4000);

      
      var server = sinon.fakeServer.create();
       
      server.respondWith('GET', '/fake/file.csv', [
        200,
        {'Content-Type': 'application/text'}, 
        'x,y,z\nbla bla,1233513.4567,"6,123,345.234"\n1,2,3'
      ]);

      window.explorerDiv = $('.data-explorer');

      vt_intl.setLanguage('fr');

      snippetHelper.buildPage (
          '/fake/file.csv',
          'csv'
      ) ;
      server.respond();

      setTimeout(function(){
        expect(window.explorerDiv.find('.even').find('.r2').html()).to.equal('1&nbsp;233&nbsp;513,4567');
        done();
      }, 500);
       
      server.restore();

    });

    it('should create a Recline dataset from fake data and display the results with the correct given decimal and thousand separators', function(done) {
      this.timeout(4000);

      
      var server = sinon.fakeServer.create();
       
      server.respondWith('GET', '/fake/file.csv', [
        200,
        {'Content-Type': 'application/text'}, 
        'x,y,z\nbla bla,1233513.4567,"6,123,345.234"\n1,2,3'
      ]);

      window.explorerDiv = $('.data-explorer');

      vt_intl.setLanguage('de');
      
      snippetHelper.buildPage (
          '/fake/file.csv',
          'csv'
      ) ;
      server.respond();

      setTimeout(function(){
        expect(window.explorerDiv.find('.even').find('.r2').html()).to.equal('1.233.513,4567');
        done();
      }, 500);
       
      server.restore();

    });

  });

  describe('createDataset from real data and URLs', function() {

    it('should create a Recline dataset from first CSV (local) URL and an extension', function(done) {
      setTimeout(done, 10000);
      
      var dataset = snippetHelper.createDataset ('data/data3.csv', 'csv') ;

      var promise = dataset.fetch();
      //expect(dataset.data).to.equal(undefined);
      promise.then(function(){
        expect(dataset.fields.length).to.equal(2);
        done();
      });

    });
    it('should create a Recline dataset from second CSV (remote) URL and an extension', function(done) {
      setTimeout(done, 10000);
      
      var dataset2 = snippetHelper.createDataset ('https://londondatastore-upload.s3.amazonaws.com/gla-elections-votes-party-borough-2004.csv', 'csv') ;

      var promise2 = dataset2.fetch();
      //expect(dataset.data).to.equal(undefined);
      promise2.then(function(){
        expect(dataset2.fields.length).to.equal(78);
        done();
      });
    });

    it('should create a Recline dataset from an RDF (local) URL and an XML extension', function(done) {
      setTimeout(done, 2000);
      
      var dataset = snippetHelper.createDataset ('data/some_rdf.xml', 'xml') ;

      var promise = dataset.fetch();
      //expect(dataset.data).to.equal(undefined);
      promise.then(function(){
        expect(dataset.fields.length).to.equal(5);
        done();
      });
    });

  });


  describe('createDataset (big file)', function() {

    it('should create a Recline dataset from a BIG XLSX (local) file URL, obtained through node backend', function(done) {
      this.timeout(30000);
      
      var dataset = snippetHelper.createDataset (
          snippetHelper.getApiSheetURL('http://localhost:8000/data/big_xlsx_65536.xlsx', 'loremmmmm'),
          'csv'
        ) ;

      var promise = dataset.fetch();
      //expect(dataset.data).to.equal(undefined);
      promise.then(function(){
        expect(dataset.fields.length).to.equal(22);
        done();
      });
    });
  });  

  describe('Internationalization', function() {
    it('should render selected terms in several appropriate languages', function() {
      vt_intl.setLanguage('fr');
      expect(vt_intl.t('Graph')).to.equal('Graphe');
      expect(vt_intl.t('test')).to.equal('testFR');
      expect(vt_intl.t('thisEntryDoesntExist')).to.equal('thisEntryDoesntExist');
      expect(vt_intl.t('testOnlyEn')).to.equal('testOnlyEn');
      vt_intl.setLanguage('en');
      expect(vt_intl.t('Graph')).to.equal('Graph');
      expect(vt_intl.t('test')).to.equal('testEN');
      expect(vt_intl.t('thisEntryDoesntExist')).to.equal('thisEntryDoesntExist');
      vt_intl.setLanguage('zz');
      expect(vt_intl.t('test')).to.equal('testEN');
    });
    it('should set the language according to the HTML element\'s lang attribute', function() {
      $('html').prop('lang', 'fr');
      vt_intl.setLanguage();
      expect(vt_intl.t('Graph')).to.equal('Graphe');
      expect(vt_intl.t('test')).to.equal('testFR');
      expect(vt_intl.t('thisEntryDoesntExist')).to.equal('thisEntryDoesntExist');
      expect(vt_intl.t('testOnlyEn')).to.equal('testOnlyEn');
      $('html').prop('lang', 'en');
      vt_intl.setLanguage();
      expect(vt_intl.t('Graph')).to.equal('Graph');
      expect(vt_intl.t('test')).to.equal('testEN');
      expect(vt_intl.t('thisEntryDoesntExist')).to.equal('thisEntryDoesntExist');
    });
  });

});