function eventFire(el, etype){
  if (el.fireEvent) {
    el.fireEvent('on' + etype);
  } else {
    var evObj = document.createEvent('Events');
    evObj.initEvent(etype, true, false);
    el.dispatchEvent(evObj);
  }
}
// test each specified CSV/XML file and checks the specified cells
describe('CSV and XML files', function () {
  var previews = document.getElementsByClassName('file-preview');
  var tests = [
    {preview:3, cell: 5, value: '0.01492'},
    {preview:4, cell: 4, value: '48'},
    {preview:11, cell: 4, value: '14'}
  ];
  tests.forEach(function(test){
    it('should load the correct CSV dataset (option '+test.preview+')', function(done){
      this.timeout(15000);
      setTimeout(function(){
        eventFire(previews[test.preview], 'click');
        setTimeout(function(){

          var cell = document.getElementsByClassName('slick-cell')[test.cell];

          expect(cell.innerHTML).to.equal(test.value);

          done();
        }, 5000);
      }, 5000);
    });
  });

});

// test each specified Excel file and checks the specified cells
describe('Excel files', function () {

  after(function(){
    eventFire(document.getElementsByClassName('overlay-open')[0], 'click');
  });

  var previews = document.getElementsByClassName('file-preview');
  var tests = [
    {preview:0, sheet: 3, cell: 30, value: '4,033,289', timeout: 2000},
    {preview:1, sheet: 11, cell: 52, value: '135,953', timeout: 2000},
    {preview:6, sheet: 1, cell: 30, value: 'lorem', timeout: 14000},
    {preview:7, sheet: 1, cell: 20, value: 'lorem', timeout: 18000},
    {preview:9, sheet: 1, cell: 20, value: 'lorem', timeout: 34000}
  ];
  tests.forEach(function(test){
    it('should load the correct Excel dataset (option '+test.preview+')', function(done){
      var timeout = 1000;
      if (test.timeout) {
        timeout = test.timeout;
      }
      this.timeout(timeout*2+3000);
      setTimeout(function(){
        eventFire(previews[test.preview], 'click');
        setTimeout(function(){
          var select = document.getElementById('sheet_names');
          select.options[test.sheet].selected = 'selected';
          eventFire(select, 'change');
          setTimeout(function(){
            var cell = document.getElementsByClassName('slick-cell')[test.cell];
            expect(cell.innerHTML).to.equal(test.value);
            done();
           }, timeout);
        }, timeout);
      }, 800);
    });
  });

});
