define(['css-loader'], function() {
  var CSS_PATH = URL_ENV + '/stylesheets/';

  // load boostrap from CDN to avoid cross-domain server problems linked to Glyphicon font
  //  loadCss(URL_ENV + '/javascripts/bootstrap/css/bootstrap.css');
  loadCss('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css');
  loadCss('https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css');

  loadCss(CSS_PATH + 'style.css');
  loadCss(CSS_PATH + 'overlay.css');
  var RECLINE_CSS_PATH = URL_ENV + '/vendor/recline/css/';
  loadCss(RECLINE_CSS_PATH + 'grid.css');
  loadCss(RECLINE_CSS_PATH + 'slickgrid.css');
  loadCss(RECLINE_CSS_PATH + 'flot.css');
  loadCss(RECLINE_CSS_PATH + 'map.css');
  loadCss(RECLINE_CSS_PATH + 'multiview.css');
  loadCss(RECLINE_CSS_PATH + 'timeline.css');

  loadCss(URL_ENV + '/vendor/recline/vendor/slickgrid/2.0.1/slick.grid.css');
  loadCss(URL_ENV + '/vendor/recline/vendor/timeline/css/timeline.css');
});
